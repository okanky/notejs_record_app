const record = require("./record");

class RecordRepository {

    constructor () { }


    /**
     * return filtered record list from records model in database
     * @param startDate
     * @param endDate
     * @param minCount
     * @param maxCount
     * @param callBack
     */
    findByCreateAtBetweenAndTotalCountBetween (startDate, endDate, minCount, maxCount,  callBack) {
        console.log('findings record with startDate: ' + startDate + ' endDate: ' + endDate + ' minCount: ' +minCount + ' maxCount: ' + maxCount);

        let query = [
            {"$match": {"createdAt": { "$gte": startDate, "$lte": endDate }}},
            {"$unwind": "$counts"},
            {"$group": {"_id": "$_id", "totalCount": { "$sum": "$counts" }, "key": { "$first": "$key" }, "createdAt": { "$first": "$createdAt" }}},
            {"$match": {"totalCount": { "$gte": minCount, "$lte": maxCount }}},
            {"$sort": {"totalCount": 1}}
        ];
        record.aggregate(query, callBack);
    }
}

module.exports = RecordRepository;