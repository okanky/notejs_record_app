const RecordRepository = require("./RecordRepository");
const recordRepository = new RecordRepository();

class RecordService {

    constructor () { }

    /**
     * return record object in callback function
     * @param recordRequestDTO
     * @param callBack
     */
    findByCreateAtBetweenAndTotalCountBetween (recordRequestDTO, callBack) {
        recordRepository.findByCreateAtBetweenAndTotalCountBetween(recordRequestDTO.startDate, recordRequestDTO.endDate, recordRequestDTO.minCount, recordRequestDTO.maxCount,  callBack)
    }
}

module.exports = RecordService;