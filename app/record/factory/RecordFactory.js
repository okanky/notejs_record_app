const  RecordResponseDTO = require('../dto/RecordResponseDTO');
class RecordService {

    constructor () { }

    /**
     * return Record response object
     * @param res
     * @param err
     * @param result
     * @returns {RecordRequestDTO}
     */
    createResponse(res, err, result){
        if (!err) {
            return new RecordResponseDTO.Builder()
                .setRecords(result)
                .setMsg("success")
                .setCode(0)
                .build();
            res.status(200);
        } else {
            return new RecordResponseDTO.Builder()
                .setMsg("err")
                .setCode(1)
                .build();
            res.status(500);
        }
    }
}

module.exports = RecordService;