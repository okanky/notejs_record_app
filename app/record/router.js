const vertex = require('vertex360')({site_id: process.env.TURBO_APP_ID});
const router = vertex.router();
const  RecordService = require('./RecordService');
const  RecordFactory = require('./factory/RecordFactory');
const  RecordRequestDTO = require('./dto/RecordRequestDTO');

const service = new RecordService();
const recordFactory = new RecordFactory();

router.post("/", (req, res) => {

	let recordRequestDTO = new RecordRequestDTO.Builder()
		.setStartDate(new Date(req.body.startDate))
		.setEndDate(new Date(req.body.endDate))
		.setMinCount(parseInt(req.body.minCount))
		.setMaxCount(parseInt(req.body.maxCount))
		.build();


	service.findByCreateAtBetweenAndTotalCountBetween(recordRequestDTO, (err, result) =>{
		res.send(recordFactory.createResponse(res,err,result));
	});
});


module.exports = router;
