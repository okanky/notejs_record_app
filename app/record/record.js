const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const objectId = Schema.Types.ObjectId;

const Record = new Schema({
    _id:objectId,
    key: { type:String, trim:true, default : ''},
    value: { type:String, trim:true, default : ''},
    createdAt: { type: Date },
    counts: { type:Number, default : 0}
});

mongoose.model("record", Record, "records");

module.exports = mongoose.model("record");