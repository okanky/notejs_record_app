class RecordRequestDTO {
    constructor(build) {
        this.startDate = build.startDate;
        this.endDate = build.endDate;
        this.minCount = build.minCount;
        this.maxCount = build.maxCount;
    }
    static get Builder() {
        class Builder {
            constructor() {
            }

            setStartDate(startDate) {
                this.startDate = startDate;
                return this;
            }
            setEndDate(endDate) {
                this.endDate = endDate;
                return this;
            }
            setMinCount(minCount) {
                this.minCount = minCount;
                return this;
            }
            setMaxCount(maxCount) {
                this.maxCount = maxCount;
                return this;
            }
            build() {
                return new RecordRequestDTO(this);
            }
        }
        return Builder;
    }
}


module.exports = RecordRequestDTO;