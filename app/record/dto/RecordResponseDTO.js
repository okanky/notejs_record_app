class RecordResponseDTO {
    constructor(build) {
        this.code = build.code;
        this.msg = build.msg;
        this.records = build.records;
    }
    static get Builder() {
        class Builder {
            constructor() {
            }

            setCode(code) {
                this.code = code;
                return this;
            }
            setMsg(msg) {
                this.msg = msg;
                return this;
            }
            setRecords(records) {
                this.records = records;
                return this;
            }
            build() {
                return new RecordResponseDTO(this);
            }
        }
        return Builder;
    }
}

module.exports = RecordResponseDTO;