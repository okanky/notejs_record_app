jest.setTimeout(30000);
const MongoClient = require('mongodb').MongoClient;

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'myproject';

describe('insert record', () => {
    let connection;
    let db;
    let record;
    let mockRecord;

    beforeAll(async () => {
        connection = await MongoClient.connect(url, {
            useNewUrlParser: true,
        });
        db = await connection.db(dbName);
        record = db.collection('records');
        mockRecord ={
            "_id" : "58adc5172a84567a19698bcb",
            "key" : "nyOpG8k775EtWyZh",
            "value" : "Hl3rKYwPW5nHY1oS3QRJVtzumkH680QnLoj6MRLXEihYCxmeAw9uwxj8JN7WCkxX5DLkVbo2aVszzXcg6becQosqhyBq92KDkOX0JU6VMBpKCvx2Qk2q3rwMaTbZMYYbFpwgq2Js4MIv",
            "createdAt" : "2016-09-10T08:36:32.119+0000",
            "counts" : [
                500,
                400
            ]
        };

    });

    afterAll(async () => {
        await connection.close();
        await db.close();
    });

    it('should insert a doc into collection', async () => {


        await record.insertOne(mockRecord);

        const insertedRecord = await record.findOne({_id: '58adc5172a84567a19698bcb'});
        expect(insertedRecord).toEqual(mockRecord);
    });

});

