
const vertex = require('vertex360')({site_id: process.env.TURBO_APP_ID})


const config = {
	views: 'views', 		// Set views directory 
	static: 'public', 		// Set static assets directory
	db: { 					// Database configuration. Remember to set env variables in .env file: MONGODB_URI, PROD_MONGODB_URI
		url: 'mongodb://dbUser:dbPassword1@ds249623.mlab.com:49623/getir-case-study',
		type: 'mongo',
		onError: (err) => {
			console.log('DB Connection Failed!')
		},
		onSuccess: () => {
			console.log('DB Successfully Connected!')
		}
	}
}

const app = vertex.app(config) // initialize app with config options



// import routes
const index = require('./app/index/router')
const api = require('./app/record/router')

// set routes
app.use('/', index)
app.use('/api', api) // sample API Routes


module.exports = app